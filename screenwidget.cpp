#include "screenwidget.h"
#include "ui_screenwidget.h"

ScreenWidget::ScreenWidget(QWidget *parent, Qt::WindowFlags f, QTextStream *kbStream) :
    QWidget(parent),
    ui(new Ui::ScreenWidget)
{    
#ifdef Q_OS_WINDOWS
    setFont(QFont("Consolas", 10));
#endif
#ifdef Q_OS_LINUX
    setFont(QFont("Monospace", 9));
#endif

    setWindowFlags(f);
    ui->setupUi(this);

    ui->textConsoleWidget->setStream(kbStream);
    ui->graphConsoleWidget->setStream(kbStream);

    ui->graphConsoleWidget->setHidden(true);
}

ScreenWidget::~ScreenWidget()
{
    delete ui;
}

void ScreenWidget::update(const Screen& s)
{
    const Screen::ScreenType ST = s.getScreenType();

    if(ST == Screen::text)
    {
        ui->textConsoleWidget->update(s);

        ui->textConsoleWidget->show();
        ui->graphConsoleWidget->hide();
    }
    else if(ST == Screen::graphics16 || s.getScreenType() == Screen::graphics64)
    {
        ui->graphConsoleWidget->update(s);

        ui->textConsoleWidget->hide();
        ui->graphConsoleWidget->show();
    }

}

void ScreenWidget::setStream(QTextStream *kbStream)
{
    ui->textConsoleWidget->setStream(kbStream);
    ui->graphConsoleWidget->setStream(kbStream);
}
