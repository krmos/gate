#include "linedelegate.h"

LineDelegate::LineDelegate(QWidget *parent) : QPlainTextEdit(parent)
{
    document()->setDocumentMargin(1);
    setOverwriteMode(true);
    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setWordWrapMode(QTextOption::NoWrap);
    setMaximumBlockCount(1);
}

void LineDelegate::keyPressEvent(QKeyEvent *e)
{
    int pos = textCursor().position();
    int key = e->key();

    if(key == Qt::Key_Up) emit changeRow(-1);
    else if(key == Qt::Key_Down) emit changeRow(1);
    else if(key == Qt::Key_Left && !pos) emit changeRow(2);
    else if(key == Qt::Key_Return || key == Qt::Key_Enter) emit editingFinished();
    else
    {
        if((pos >= 0 && pos < 16) || key == Qt::Key_Left)
        {
            QPlainTextEdit::keyPressEvent(e);

            int ancPos = textCursor().anchor();
            pos = textCursor().position();

            if(pos == 16 && pos == ancPos) emit changeRow(1);
        }
        else e->ignore();
    }
}

