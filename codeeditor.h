#ifndef CODEEDITOR_H
#define CODEEDITOR_H

#include <QPlainTextEdit>
#include <QKeyEvent>
#include <QPainter>
#include <QTextBlock>
#include <QDebug>
#include <QList>

class CodeEditor : public QPlainTextEdit
{
    Q_OBJECT
public:
    CodeEditor(QWidget *parent = 0, QList<int>* _opLenghts = nullptr);

    void lineNumberAreaPaintEvent(QPaintEvent *event);
    int lineNumberAreaWidth();

    void setOpLenghts(QList<int>* _opLenghts);
private:
    class LineNumberArea : public QWidget
    {
    public:
        LineNumberArea(CodeEditor *editor) : QWidget(editor) {
#ifdef Q_OS_WINDOWS
            setFont(QFont("Consolas", 10));
#endif
#ifdef Q_OS_LINUX
            setFont(QFont("Monospace", 9));
#endif
            codeEditor = editor;
        }

        QSize sizeHint() const Q_DECL_OVERRIDE {
            return QSize(codeEditor->lineNumberAreaWidth(), 0);
        }

    protected:
        void paintEvent(QPaintEvent *event) Q_DECL_OVERRIDE {
            codeEditor->lineNumberAreaPaintEvent(event);
        }

    private:
        CodeEditor *codeEditor;
    };

    bool codeChanged = false;
    QTextBlock lastBlock = firstVisibleBlock();
    LineNumberArea *lineNumberArea;
    QList<int>* opLenghts;
protected:
    void resizeEvent(QResizeEvent *event) Q_DECL_OVERRIDE;
signals:
    void codeUpdated();
private slots:
    void highlightCurrentLine();
    void updateLineNumberArea(const QRect &rect, int dy);
    void updateNeeded();
};

#endif // CODEEDITOR_H
