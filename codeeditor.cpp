#include "codeeditor.h"


CodeEditor::CodeEditor(QWidget* parent, QList<int> *_opLenghts) : QPlainTextEdit(parent),
    opLenghts(_opLenghts)
{
    lineNumberArea = new LineNumberArea(this);

#ifdef Q_OS_WIN32
    setFont(QFont("Consolas", 10));
#endif
#ifdef Q_OS_LINUX
    setFont(QFont("Monospace", 9));
#endif

    connect(this, SIGNAL(updateRequest(QRect,int)), this, SLOT(updateLineNumberArea(QRect,int)));
    connect(this, SIGNAL(cursorPositionChanged()), this, SLOT(highlightCurrentLine()));
    connect(this, SIGNAL(textChanged()), this, SLOT(updateNeeded()));

    setViewportMargins(lineNumberAreaWidth(), 0, 0, 0);
    highlightCurrentLine();

    setMinimumWidth(lineNumberAreaWidth() + (fontMetrics().width('0') * 34));
}

void CodeEditor::lineNumberAreaPaintEvent(QPaintEvent *event)
{
    QPainter painter(lineNumberArea);
    painter.fillRect(event->rect(), Qt::lightGray);
    QTextBlock block = firstVisibleBlock();

    int blockNumber = block.blockNumber();
    int top = (int) blockBoundingGeometry(block).translated(contentOffset()).top();
    int bottom = top + (int) blockBoundingRect(block).height();

    QList<int>::iterator i = opLenghts->begin();

    int address = 0x100;

    for(int j = 0; j < blockNumber; ++j)
    {
        address += *i;
        ++i;
    }

    while (block.isValid() && top <= event->rect().bottom()) {
        if (block.isVisible() && bottom >= event->rect().top()) {
            QString number = QString::number(address, 16).toUpper().rightJustified(4, '0');
            number.prepend(" CS:");
            number.append(' ');
            painter.setPen(Qt::black);
            painter.drawText(0, top, lineNumberArea->width(), fontMetrics().height(),
                             Qt::AlignRight, number);
        }

        if(i != opLenghts->end())
        {
            address += *i;
            ++i;
        }

        block = block.next();
        top = bottom;
        bottom = top + (int) blockBoundingRect(block).height();
        ++blockNumber;
    }
}

int CodeEditor::lineNumberAreaWidth()
{
    int digits = 9;

    int space = fontMetrics().width('0') * digits;

    return space;
}

void CodeEditor::setOpLenghts(QList<int> *_opLenghts)
{
    opLenghts = _opLenghts;
}

void CodeEditor::resizeEvent(QResizeEvent *event)
{
    QPlainTextEdit::resizeEvent(event);

    QRect cr = contentsRect();
    lineNumberArea->setGeometry(QRect(cr.left(), cr.top(), lineNumberAreaWidth(), cr.height()));
}

void CodeEditor::highlightCurrentLine()
{
    QList<QTextEdit::ExtraSelection> extraSelections;

    if (!isReadOnly()) {
        QTextEdit::ExtraSelection selection;

        QColor lineColor = QColor(Qt::yellow).lighter(160);

        selection.format.setBackground(lineColor);
        selection.format.setProperty(QTextFormat::FullWidthSelection, true);
        selection.cursor = textCursor();
        selection.cursor.clearSelection();
        extraSelections.append(selection);
    }

    if(textCursor().block() != lastBlock)
    {
        if(codeChanged)
        {
            codeChanged = false;
            emit codeUpdated();
            show();
        }
        lastBlock = textCursor().block();
    }

    setExtraSelections(extraSelections);
}

void CodeEditor::updateLineNumberArea(const QRect &rect, int dy)
{
    if (dy)
        lineNumberArea->scroll(0, dy);
    else
        lineNumberArea->update(0, rect.y(), lineNumberArea->width(), rect.height());
}

void CodeEditor::updateNeeded()
{
    codeChanged = true;
}
