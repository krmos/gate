#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    fileDialog(new QFileDialog(this)),
    asciiTable(new QTableView(this)),
    hexConverter(new HexConverter(this)),
    helpViewer(new QTextBrowser(this)),
    memory(new QByteArray(MemSize, 0x0)),
    debug(new DebuggerUI(this, Qt::Window)),
    compiler(new Compiler())
{
    registers = {
        {"CS", Register(0x13f4)},
        {"DS", Register(0x13f4)},
        {"SS", Register(0x13f4)},
        {"ES", Register(0xFFF0)},
        {"SP", Register(0xFFFE)},
        {"IP", Register(0x0100)}
    };

    ui->setupUi(this);
    ui->DShexEdit->init(memory, registers["DS"].get());
    ui->warnErrField->setHidden(true);

    ui->mainTextField->setOpLenghts(&opLengths);

    hexConverter->setWindowFlags(Qt::Window);    
    hexConverter->setWindowTitle("Hex Converter");

    helpViewer->setWindowFlags(Qt::Window);
    helpViewer->setWindowTitle("Help");
    helpViewer->setSource(QUrl("help/index.htm"));
    helpViewer->setMinimumWidth(320);
    helpViewer->setMinimumHeight(240);
    helpViewer->resize(800, 625);
    helpViewer->document()->setDocumentMargin(0);

    asciiTable->setWindowFlags(Qt::Window);
    asciiTable->setWindowTitle("ASCII Table");

    connect(ui->mainTextField, SIGNAL(codeUpdated()), this, SLOT(updateCS()));
    connect(this, SIGNAL(fileNameChanged(bool)), this, SLOT(updateTitle(bool)));
}

MainWindow::~MainWindow()
{
    delete compiler;
    delete debug;
    delete memory;
    delete fileDialog;
    delete helpViewer;
    delete asciiTable;
    delete hexConverter;
    delete ui;
}

void MainWindow::on_actionExit_triggered()
{
    this->close();
}

void MainWindow::on_actionOpen_triggered()
{
    if(unsavedChanges)
    {
        switch( QMessageBox::information(
                    this, "Unsaved Changes",
                    "The project contains unsaved changes\n"
                    "Do you want to save the changes before continuing?",
                    "&Save", "&Discard", "Cancel",
                    0,      // Enter == button 0
                    2 ) ) { // Escape == button 2
        case 0:
            ui->actionSave->trigger();
            break;
        case 1:
            break;
        case 2:
            return;
        }
    }

    QString fileName =
            QFileDialog::getOpenFileName(
                this, tr("Open Project"), projectPath, tr("Assembly files (*.asm)"));

    openFile(fileName);

    unsavedChanges = false;
}

bool MainWindow::openFile(QString fileName)
{
    QFile projectFile(fileName);
    projectFile.open(QIODevice::ReadOnly);

    if(projectFile.isOpen())
    {
        QDataStream in(&projectFile);

        int segment = (registers["DS"].get() << 4);

        memory->clear();
        memory->resize(MemSize);

        QByteArray temp;

        in >> temp;

        memory->remove(segment, temp.size());
        memory->insert(segment, temp);

        QString text;
        in >> text;

        ui->mainTextField->setPlainText(text);

        ui->DShexEdit->reload();
        updateCS();

        projectFile.close();

        projectFileName = fileName;
        projectPath = QFileInfo(projectFile).path();

        emit fileNameChanged();

        return false;
    }
    else return true ;
}

bool MainWindow::writeFile(QString fileName)
{
    QFile projectFile(fileName);
    projectFile.open(QIODevice::WriteOnly);

    if(projectFile.isOpen())
    {
        QDataStream out(&projectFile);

        int segment = (registers["DS"].get() << 4);

        QByteArray temp = memory->mid(segment, 0xFFFF);

        out << temp;

        out << ui->mainTextField->toPlainText();

        projectFile.close();

        projectFileName = fileName;
        projectPath = QFileInfo(projectFile).path();

        emit fileNameChanged();

        unsavedChanges = false;

        return false;
    }
    else return true ;
}

bool MainWindow::updateCS()
{
    ui->warnErrField->clear();

    QString str = ui->mainTextField->toPlainText();
    QStringList strList = str.split('\n');

    QStringList errList;

    ui->DShexEdit->update(compiler->compile(strList, opLengths, errList));

    if(!errList.empty())
    {
        ui->warnErrField->show();
        ui->warnErrField->addItems(errList);
        return false;
    }
    ui->warnErrField->setHidden(true);

    return true;
}

void MainWindow::on_actionSave_triggered()
{
    QString fileName = (projectFileName.isEmpty())
            ? QFileDialog::getSaveFileName(
                this, tr("Save Project As"), projectPath, tr("Assembly files (*.asm)"))
            : projectFileName;

    writeFile(fileName);
}

void MainWindow::on_actionSave_As_triggered()
{
    QString fileName =
            QFileDialog::getSaveFileName(
                this, tr("Save Project As"), projectPath, tr("Assembly files (*.asm)"));

        writeFile(fileName);
}

void MainWindow::on_actionNew_triggered()
{
    if(unsavedChanges)
    {
        switch( QMessageBox::information(
                    this, "Unsaved Changes",
                    "The project contains unsaved changes\n"
                    "Do you want to save the changes before continuing?",
                    "&Save", "&Discard", "Cancel",
                    0,      // Enter == button 0
                    2 ) ) { // Escape == button 2
        case 0:
            ui->actionSave->trigger();
            break;
        case 1:
            break;
        case 2:
            return;
        }
    }

    projectFileName.clear();
    projectPath = "/";

    memory->clear();
    memory->resize(MemSize);

    ui->mainTextField->setPlainText("");
    ui->DShexEdit->init(memory, registers["DS"].get());

    updateCS();

    emit fileNameChanged(true);

    unsavedChanges = false;
}

void MainWindow::on_mainTextField_textChanged()
{
    unsavedChanges = true;
}

void MainWindow::on_actionDebugger_triggered()
{
    if(updateCS())
    {
        QStringList strings = ui->mainTextField->toPlainText().split((QChar('\n')));

        QRegExp empty("[ \t]*");

        int address = 0x100;

        QVector<QPair<int, QString>> lines;

        auto j = opLengths.begin();

        for(int i = 0; i < strings.size(); ++i)
        {
            QString temp = strings.at(i).mid(0, strings.at(i).indexOf(';')).toUpper();

            if(j != opLengths.end())
            {
                if(!empty.exactMatch(temp))
                {
                    lines.append(QPair<int, QString>(address, temp));
                    address += *j;
                    ++j;
                }
            }
            else break;
        }
        lines.append(QPair<int, QString>(address, QString()));

        debug->init(lines, memory, &registers);
        debug->show();
    }
}

void MainWindow::asciiInit()
{
    asciiTable->setShowGrid(false);
    asciiTable->setAlternatingRowColors(true);

    const int Rows = 16;
    const int Columns = 16;

    asciiTable->horizontalHeader()->hide();
    asciiTable->verticalHeader()->hide();

    QStandardItemModel* temp = new QStandardItemModel(Rows, Columns, this);

#ifdef Q_OS_WIN32
    asciiTable->setFont(QFont("Consolas", 10));
#endif
#ifdef Q_OS_LINUX
    asciiTable->setFont(QFont("Monospace", 9));
#endif

    QTextCodec* cyrillic = QTextCodec::codecForName("CP-866");

    for(int i = 0; i < Rows; ++i)
    {
        for(int j = 0; j < Columns; ++j)
        {
            QString str = QString::number((j * 16) + i, 16).toUpper().rightJustified(2, '0');
            str.append(' ');
            const char* ch = new char((j * 16) + i);
            str.append(cyrillic->toUnicode(ch, 1));

            QStandardItem* item = new QStandardItem(str);
            temp->setItem(i, j, item);
            delete ch;
        }
    }

    asciiTable->verticalHeader()->setDefaultSectionSize(20);
    asciiTable->horizontalHeader()->setDefaultSectionSize(45);

    asciiTable->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    asciiTable->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

    asciiTable->setMinimumSize(45 * Columns + 2, 20 * Rows + 2);

    asciiTable->setModel(temp);

    asciiGenerated = true;
}

void MainWindow::closeEvent(QCloseEvent *e)
{
    if(unsavedChanges)
    {
        switch( QMessageBox::information(
                    this, "Unsaved Changes",
                    "The project contains unsaved changes\n"
                    "Do you want to save the changes before exiting?",
                    "&Save", "&Discard", "Cancel",
                    0,      // Enter == button 0
                    2 ) ) { // Escape == button 2
        case 0:
            ui->actionSave->trigger();
            break;
        case 1:
            e->accept();
            break;
        case 2:
            e->ignore();
            return;
        }
    }
}

void MainWindow::on_actionASCII_Table_triggered()
{
    if(!asciiGenerated) asciiInit();

    asciiTable->show();
}

void MainWindow::on_actionHex_Converter_triggered()
{
    hexConverter->show();
}

void MainWindow::on_actionHelp_Index_triggered()
{
    helpViewer->show();
}

void MainWindow::updateTitle(bool reset)
{
    if(reset) setWindowTitle("GATE - New Project");
    else setWindowTitle(QString("GATE - " + projectFileName));
}
