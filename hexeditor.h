#ifndef HEXEDITOR_H
#define HEXEDITOR_H

#include <QTableWidget>
#include <QHeaderView>
#include <QKeyEvent>
#include <QDebug>
#include <QTextCodec>
#include "asciidelegate.h"

class HexEditor : public QTableWidget
{
    Q_OBJECT

public:
    HexEditor(QWidget *parent);
    ~HexEditor();

    void keyPressEvent(QKeyEvent *e);
    void mouseDoubleClickEvent(QMouseEvent *event);

    void init(QByteArray* mem, unsigned int segStart);
    void initAndLoad(QByteArray* mem, unsigned int segStart);
    void reload();
    void update(const QByteArray &ba);

    void setReadOnly(const bool& b);
    void setShowAscii(const bool& b);
private slots:
    void tableClicked(int row, int column);
    void currentCellChanged(int currentRow, int currentColumn, int previousRow, int previousColumn);
    void delegateEditorClosed();
    void rowChanged(int r);
public slots:
    void refresh(QByteArray ba, int pos);
private:
    QByteArray* memory;

    unsigned int segmentStart;

    QString segmentRegName;

    AsciiDelegate* delegate;

    QTextCodec* cyrillic;

    bool readOnly = false;
    bool showAscii = true;

    const int Rows = 0xFF0;
    const int Columns = 17;
    const int Offset = 0x100;

    int previousSegLength = 0;

    bool changeSelection(int row, int col);

    QByteArray toHex(QString str);
    QString toReadable(QByteArray ba);

    int cur = 0;
};

#endif // HEXEDITOR_H
