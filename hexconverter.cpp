#include "hexconverter.h"
#include "ui_hexconverter.h"

HexConverter::HexConverter(QWidget *parent) :
    QWidget(parent),

    sByte(new QIntValidator(-128, 127)),
    sShort(new QIntValidator(-32768, 32767)),
    sInt(new QIntValidator()),

    uByte(new QIntValidator(0, 255)),
    uShort(new QIntValidator(0, 65535)),
    uInt(new QIntValidator(0, 4294967295)),

    hByte(new QRegExpValidator(QRegExp("^[0-9A-Fa-f]{1,2}$"))),
    hShort(new QRegExpValidator(QRegExp("^[0-9A-Fa-f]{1,4}$"))),
    hInt(new QRegExpValidator(QRegExp("^[0-9A-Fa-f]{1,8}$"))),

    ui(new Ui::HexConverter)
{
    ui->setupUi(this);

    connect(ui->hex, SIGNAL(textEdited(QString)), this, SLOT(hexChanged(QString)));
    connect(ui->dec, SIGNAL(textEdited(QString)), this, SLOT(decChanged(QString)));

    ui->dec->setValidator(sShort);
    ui->hex->setValidator(hShort);

    ui->signSwitch->addItem("Signed", 1);
    ui->signSwitch->addItem("Unsigned", 0);

    ui->hex->setMaxLength(4);
}

HexConverter::~HexConverter()
{
    delete sByte;
    delete sShort;
    delete sInt;

    delete uByte;
    delete uShort;
    delete uInt;

    delete hByte;
    delete hShort;
    delete hInt;

    delete ui;
}

void HexConverter::hexChanged(QString text)
{    
    if(ui->signSwitch->currentIndex() == 0)
    {
        if(sType == byte)
        {
            char arg = text.toUInt(0, 16);
            ui->dec->setText(QString::number(arg, 10));
        }
        else if(sType == word)
        {
            short arg = text.toUInt(0, 16);
            ui->dec->setText(QString::number(arg, 10));
        }
        else if(sType == dword)
        {
            int arg = text.toUInt(0, 16);
            ui->dec->setText(QString::number(arg, 10));
        }
    }
    else
    {
        if(sType == byte)
        {
            unsigned char arg = text.toUInt(0, 16);
            ui->dec->setText(QString::number(arg, 10));
        }
        else if(sType == word)
        {
            unsigned short arg = text.toUInt(0, 16);
            ui->dec->setText(QString::number(arg, 10));
        }
        else if(sType == dword)
        {
            unsigned int arg = text.toUInt(0, 16);
            ui->dec->setText(QString::number(arg, 10));
        }
    }

    lastEdited = ui->hex;
}

void HexConverter::decChanged(QString text)
{
    if(sType == byte)
    {
        char arg = text.toInt(0, 10);
        ui->hex->setText(QString::number(static_cast<unsigned char>(arg), 16).toUpper().rightJustified(ui->hex->maxLength(), '0'));
    }
    else if(sType == word)
    {
        short arg = text.toInt(0, 10);
        ui->hex->setText(QString::number(static_cast<unsigned short>(arg), 16).toUpper().rightJustified(ui->hex->maxLength(), '0'));
    }
    else if(sType == dword)
    {
        int arg = text.toInt(0, 10);
        ui->hex->setText(QString::number(static_cast<unsigned int>(arg), 16).toUpper().rightJustified(ui->hex->maxLength(), '0'));
    }

    lastEdited = ui->dec;
}

void HexConverter::on_rbyte_clicked()
{
    const int hexWidth = 2;

    sType = byte;

    if(ui->signSwitch->currentIndex() == 0) ui->dec->setValidator(sByte);
    else if(ui->signSwitch->currentIndex() == 1) ui->dec->setValidator(uByte);

    ui->hex->setValidator(hByte);

    QString temp = ui->hex->text().mid(ui->hex->text().length() - 2);
    ui->hex->setText(temp);

    ui->hex->setMaxLength(hexWidth);

    update();
}

void HexConverter::on_rword_clicked()
{
    const int hexWidth = 4;

    sType = word;

    if(ui->signSwitch->currentIndex() == 0) ui->dec->setValidator(sShort);
    else if(ui->signSwitch->currentIndex() == 1) ui->dec->setValidator(uShort);

    ui->hex->setValidator(hShort);

    QString temp = ui->hex->text().mid(ui->hex->text().length() - 4);
    ui->hex->setText(temp);

    ui->hex->setMaxLength(hexWidth);

    update();
}

void HexConverter::on_rdword_clicked()
{
    const int hexWidth = 8;

    sType = dword;

    if(ui->signSwitch->currentIndex() == 0) ui->dec->setValidator(sInt);
    else if(ui->signSwitch->currentIndex() == 1) ui->dec->setValidator(uInt);

    ui->hex->setValidator(hInt);
    ui->hex->setMaxLength(hexWidth);

    update();
}

void HexConverter::update()
{
    if(lastEdited == ui->hex) hexChanged(ui->hex->text());

    if(ui->signSwitch->currentIndex() == 0)
    {
        if(sType == byte) ui->dec->setValidator(sByte);
        else if(sType == word) ui->dec->setValidator(sShort);
        else if(sType == dword) ui->dec->setValidator(sInt);
    }
    else if(ui->signSwitch->currentIndex() == 1)
    {
        if(sType == byte) ui->dec->setValidator(uByte);
        else if(sType == word) ui->dec->setValidator(uShort);
        else if(sType == dword) ui->dec->setValidator(uInt);
    }
}

void HexConverter::on_signSwitch_currentIndexChanged(int index)
{
    if(lastEdited == ui->hex) hexChanged(lastEdited->text());
    update();
}
