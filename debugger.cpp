#include "debugger.h"

Debugger::Debugger(QObject *parent, QTextStream *kbStream) :
    QObject(parent), kbStream(kbStream){
    registers = {
        {"AX", Register()},
        {"CX", Register()},
        {"DX", Register()},
        {"BX", Register()},
        {"SI", Register()},
        {"DI", Register()},
        {"SP", Register()},
        {"BP", Register()},
        {"IP", Register()},
        {"CS", Register()},
        {"DS", Register()},
        {"ES", Register()},
        {"SS", Register()}
    };
    instructions = {
        {"MOV", &Debugger::mov},
        {"ADD", &Debugger::add},
        {"SUB", &Debugger::sub},
        {"MUL", &Debugger::mul},
        {"DIV", &Debugger::div},
        {"INC", &Debugger::inc},
        {"DEC", &Debugger::dec},
        {"IMUL", &Debugger::imul},
        {"IDIV", &Debugger::idiv},
        {"PUSH", &Debugger::push},
        {"POP", &Debugger::pop},
        {"INT", &Debugger::interrupt},
        {"CMP", &Debugger::cmp},
        {"JMP", &Debugger::jmp},
        {"JE", &Debugger::je},
        {"JZ", &Debugger::jz},
        {"JNE", &Debugger::jne},
        {"JNZ", &Debugger::jnz},
        {"JG", &Debugger::jg},
        {"JL", &Debugger::jl},
        {"JGE", &Debugger::jge},
        {"JLE", &Debugger::jle}
    };
    flags = {
        {"CF", false},
        {"ZF", false},
        {"SF", false},
        {"OF", false}
    };
}

Debugger::~Debugger() {}

void Debugger::terminateASAP()
{
    terminate = true;
}

void Debugger::next()
{
    nextBP = true;
}

void Debugger::mov(const opArgs args)
{
    resetFlags();

    aType argT1 = getArgType(args[0]);
    aType argT2 = getArgType(args[1]);

    int n = 0;

    if(argT1 == reg)
    {
        if(argT2 == num) n = args[1].toInt(0 , Base);
        else if(argT2 == reg) n = getRegVal(args[1]);
        else if(argT2 == mem)
        {
            if(args[0].endsWith('H') || args[0].endsWith('L')) n = getMemVal(args[1], 0);
            else n = getMemVal(args[1], 1) ;
        }
        else qDebug() << "Unsupported argument";

        setRegVal(args[0], n);
    }
    else if (argT1 == mem)
    {
        if(argT2 == reg)
        {
            n = getRegVal(args[1]);

            if(args[1].endsWith('H') || args[1].endsWith('L')) setMemVal(args[0], n, 0);
            else setMemVal(args[0], n, 1);
        }
        else qDebug() << "Unsupported argument";

    }
    else qDebug() << "Unsupported argument";
}

void Debugger::add(const opArgs args)
{
    resetFlags();

    aType argT1 = getArgType(args[0]);
    aType argT2 = getArgType(args[1]);

    if(argT1 == reg)
    {
        if(args[0].endsWith('H') || args[0].endsWith('L'))
        {
            unsigned char un1 = getRegVal(args[0]);
            unsigned char un2 = 0;
            signed char n1 = un1;
            signed char n2 = 0;

            if(argT2 == num) n2 = un2 =  args[1].toInt(0 , Base);
            else if(argT2 == reg) n2 = un2 = getRegVal(args[1]);
            else if (argT2 == mem) n2 = un2 = getMemVal(args[1], 0);
            else qDebug() << "Unsupported argument";

            signed short res = (n1 + n2);
            unsigned short ures = (un1 + un2);

            signed char r = res;
            unsigned char ur = res;

            flags["OF"] = (res > CHAR_MAX || res < CHAR_MIN);
            flags["CF"] = (ur != ures);
            flags["SF"] = (r < 0);
            flags["ZF"] = (r == 0);

            setRegVal(args[0], ur);
        }
        else
        {
            unsigned short un1 = getRegVal(args[0]);
            unsigned short un2 = 0;
            signed short n1 = un1;
            signed short n2 = 0;

            if(argT2 == num) n2 = un2 = args[1].toInt(0 , Base);
            else if(argT2 == reg) n2 = un2 = getRegVal(args[1]);
            else if (argT2 == mem) n2 = un2 = getMemVal(args[1], 1);
            else qDebug() << "Unsupported argument";

            signed int res = (n1 + n2);
            unsigned int ures = (un1 + un2);

            signed short r = res;
            unsigned short ur = res;

            flags["OF"] = (res > SHRT_MAX || res < SHRT_MIN);
            flags["CF"] = (ur != ures);
            flags["SF"] = (r < 0);
            flags["ZF"] = (r == 0);

            setRegVal(args[0], ur);
        }
    }
    else if (argT1 == mem)
    {
        if(argT2 == reg)
        {
            if(args[1].endsWith('H') || args[1].endsWith('L'))
            {
                unsigned char un1 = getRegVal(args[1]);
                unsigned char un2 = getMemVal(args[0], 0);
                signed char n1 = un1;
                signed char n2 = un2;

                signed short res = (n1 + n2);
                unsigned short ures = (un1 + un2);

                signed char r = res;
                unsigned char ur = res;

                flags["OF"] = (res > CHAR_MAX || res < CHAR_MIN);
                flags["CF"] = (ur != ures);
                flags["SF"] = (r < 0);
                flags["ZF"] = (r == 0);

                setMemVal(args[0], ur, 0);
            }
            else
            {
                unsigned short un1 = getRegVal(args[1]);
                unsigned short un2 = getMemVal(args[0], 1);
                signed short n1 = un1;
                signed short n2 = un2;

                signed int res = (n1 + n2);
                unsigned int ures = (un1 + un2);

                signed short r = res;
                unsigned short ur = res;

                flags["OF"] = (res > SHRT_MAX || res < SHRT_MIN);
                flags["CF"] = (ur != ures);
                flags["SF"] = (r < 0);
                flags["ZF"] = (r == 0);

                setMemVal(args[0], ur, 1);
            }
        }
        else qDebug() << "Unsupported argument";
    }
    else qDebug() << "Unsupported argument";
}

void Debugger::sub(const opArgs args)
{
    resetFlags();

    aType argT1 = getArgType(args[0]);
    aType argT2 = getArgType(args[1]);

    if(argT1 == reg)
    {
        if(args[0].endsWith('H') || args[0].endsWith('L'))
        {
            unsigned char un1 = getRegVal(args[0]);
            unsigned char un2 = 0;
            signed char n1 = un1;
            signed char n2 = 0;

            if(argT2 == num) n2 = un2 = args[1].toInt(0 , Base);
            else if(argT2 == reg) n2 = un2 = getRegVal(args[1]);
            else if (argT2 == mem) n2 = un2 = getMemVal(args[1], 0);
            else qDebug() << "Unsupported argument";

            signed short res = (n1 - n2);
            unsigned short ures = (un1 - un2);

            signed char r = res;
            unsigned char ur = res;

            flags["OF"] = (res > CHAR_MAX || res < CHAR_MIN);
            flags["CF"] = (ur != ures);
            flags["SF"] = (r < 0);
            flags["ZF"] = (r == 0);

            setRegVal(args[0], ur);
        }
        else
        {
            unsigned short un1 = getRegVal(args[0]);
            unsigned short un2 = 0;
            signed short n1 = un1;
            signed short n2 = 0;

            if(argT2 == num) n2 = un2 = args[1].toInt(0 , Base);
            else if(argT2 == reg) n2 = un2 = getRegVal(args[1]);
            else if (argT2 == mem) n2 = un2 = getMemVal(args[1], 1);
            else qDebug() << "Unsupported argument";

            signed int res = (n1 - n2);
            unsigned int ures = (un1 - un2);

            signed short r = res;
            unsigned short ur = res;

            flags["OF"] = (res > SHRT_MAX || res < SHRT_MIN);
            flags["CF"] = (ur != ures);
            flags["SF"] = (r < 0);
            flags["ZF"] = (r == 0);

            setRegVal(args[0], ur);
        }
    }
    else if (argT1 == mem)
    {
        if(argT2 == reg)
        {
            if(args[1].endsWith('H') || args[1].endsWith('L'))
            {
                unsigned char un1 = getRegVal(args[1]);
                unsigned char un2 = getMemVal(args[0], 0);
                signed char n1 = un1;
                signed char n2 = un2;

                signed short res = (n1 - n2);
                unsigned short ures = (un1 - un2);

                signed char r = res;
                unsigned char ur = res;

                flags["OF"] = (res > CHAR_MAX || res < CHAR_MIN);
                flags["CF"] = (ur != ures);
                flags["SF"] = (r < 0);
                flags["ZF"] = (r == 0);

                setMemVal(args[0], ur, 0);
            }
            else
            {
                unsigned short un1 = getRegVal(args[1]);
                unsigned short un2 = getMemVal(args[0], 1);
                signed short n1 = un1;
                signed short n2 = un2;

                signed int res = (n1 - n2);
                unsigned int ures = (un1 - un2);

                signed short r = res;
                unsigned short ur = res;

                flags["OF"] = (res > SHRT_MAX || res < SHRT_MIN);
                flags["CF"] = (ur != ures);
                flags["SF"] = (r < 0);
                flags["ZF"] = (r == 0);

                setMemVal(args[0], ur, 1);
            }
        }
        else qDebug() << "Unsupported argument";
    }
    else qDebug() << "Unsupported argument";
}

void Debugger::mul(const Debugger::opArgs args)
{
    resetFlags();

    aType argT1 = getArgType(args[0]);

    if(argT1 == reg)
    {
        if(args[0].endsWith('H') || args[0].endsWith('L'))
        {
            unsigned char n1 = getRegVal("AL");
            unsigned char n2 = getRegVal(args[0]);
            unsigned short res = n1 * n2;

            registers["AX"].set(res);
        }
        else
        {
            unsigned short n1 = getRegVal("AX");
            unsigned short n2 = getRegVal(args[0]);
            unsigned int res = n1 * n2;

            registers["DX"].set((res & 0xffff0000) >> 16);
            registers["AX"].set(res & 0x0000ffff);
        }
    }
    else qDebug() << "Unsupported argument";
}

void Debugger::div(const Debugger::opArgs args)
{
    resetFlags();

    aType argT1 = getArgType(args[0]);

    if(argT1 == reg)
    {
        if(args[0].endsWith('H') || args[0].endsWith('L'))
        {
            unsigned char divisor = getRegVal(args[0]);
            if(divisor)
            {
                unsigned short dividend = getRegVal("AX");

                unsigned short quotient = dividend / divisor;
                unsigned short remainder = dividend % divisor;

                if(quotient <= UCHAR_MAX && remainder <= UCHAR_MAX)
                {
                    registers["AX"].setHigh(remainder);
                    registers["AX"].setLow(quotient);
                }
                else *errStream << "Target registry overflow.";
            }
            else *errStream << "Division by zero.";
        }
        else
        {
            unsigned short divisor = getRegVal(args[0]);
            if(divisor)
            {
                unsigned int dividend = (getRegVal("AX") & 0x0000ffff);
                dividend |= (0xffff0000 & (getRegVal("DX") << 16));

                unsigned int quotient = dividend / divisor;
                unsigned int remainder = dividend % divisor;

                if(quotient <= USHRT_MAX && remainder <= USHRT_MAX)
                {
                    registers["AX"].set(quotient);
                    registers["DX"].set(remainder);
                }
                else *errStream << "Target registry overflow.";
            }
            else *errStream << "Division by zero.";
        }
    }
    else qDebug() << "Unsupported argument";
}

void Debugger::imul(const Debugger::opArgs args)
{
    resetFlags();

    aType argT1 = getArgType(args[0]);

    if(argT1 == reg)
    {
        if(args[0].endsWith('H') || args[0].endsWith('L'))
        {
            char n1 = getRegVal("AL");
            char n2 = getRegVal(args[0]);
            short res = n1 * n2;

            registers["AX"].set(res);
        }
        else
        {
            short n1 = getRegVal("AX");
            short n2 = getRegVal(args[0]);
            int res = n1 * n2;

            registers["DX"].set((res & 0xffff0000) >> 16);
            registers["AX"].set(res & 0x0000ffff);
        }
    }
    else qDebug() << "Unsupported argument";
}

void Debugger::idiv(const Debugger::opArgs args)
{
    resetFlags();

    aType argT1 = getArgType(args[0]);

    if(argT1 == reg)
    {
        if(args[0].endsWith('H') || args[0].endsWith('L'))
        {
            char divisor = getRegVal(args[0]);

            if(divisor)
            {
                short dividend = getRegVal("AX");
                short quotient = dividend / divisor;
                short remainder = dividend % divisor;

                if(quotient >= CHAR_MIN && quotient <= CHAR_MAX
                        && remainder >= CHAR_MIN && quotient <= CHAR_MAX)
                {
                    registers["AX"].setHigh(remainder);
                    registers["AX"].setLow(quotient);
                }
                else *errStream << "Target registry overflow.";
            }
            else *errStream << "Division by zero.";
        }
        else
        {
            short divisor = getRegVal(args[0]);

            if(divisor)
            {
                int dividend = (getRegVal("AX") & 0x0000ffff);
                dividend |= (0xffff0000 & (getRegVal("DX") << 16));
                int quotient = dividend / divisor;
                int remainder = dividend % divisor;

                if(quotient >= SHRT_MIN && quotient <= SHRT_MAX
                        && remainder >= SHRT_MIN && quotient <= SHRT_MAX)
                {
                    registers["AX"].set(quotient);
                    registers["DX"].set(remainder);
                }
                else *errStream << "Target registry overflow.";
            }
            else *errStream << "Division by zero.";
        }
    }
    else qDebug() << "Unsupported argument";

}

void Debugger::push(const Debugger::opArgs args)
{
    aType argT1 = getArgType(args[0]);

    if(argT1 == reg)
    {
        if(!(args[0].endsWith('H') || args[0].endsWith('L')))
        {
            QByteArray temp;

            temp.append(registers[args[0]].getLow());
            temp.append(registers[args[0]].getHigh());

            int segment = (registers["SS"].get() << 4);
            int pointer = registers["SP"].get() - 2;

            memory.replace(segment + pointer, temp.length(), temp);
            emit MemoryUpdated(temp, pointer);

            registers["SP"].set(pointer);
        }
        else qDebug() << "Unsupported argument";
    }
    else qDebug() << "Unsupported argument";
}

void Debugger::pop(const Debugger::opArgs args)
{
    aType argT1 = getArgType(args[0]);

    if(argT1 == reg)
    {
        if(!(args[0].endsWith('H') || args[0].endsWith('L')))
        {
            QByteArray temp(2, 0);

            int segment = (registers["SS"].get() << 4);
            int pointer = registers["SP"].get();

            int begin = pointer;

            registers[args[0]].setLow(memory.at(segment + pointer));
            registers[args[0]].setHigh(memory.at(segment + ++pointer));

            memory.replace(segment + begin, temp.length(), temp);
            emit MemoryUpdated(temp, begin);

            registers["SP"].set(++pointer);
        }
        else qDebug() << "Unsupported argument";
    }
    else qDebug() << "Unsupported argument";
}

void Debugger::inc(const Debugger::opArgs args)
{
    auto cf = flags.value("CF"); //CF - unchanged !!!
    resetFlags();
    flags["CF"] = cf;

    aType argT1 = getArgType(args[0]);

    QString temp;

    if(argT1 == reg)
    {
        if (args[0].endsWith('H'))
        {
            short n = registers[temp + args[0].at(0) + 'X'].getHigh();
            ++n;
            flags["OF"] = (n > CHAR_MAX || n < CHAR_MIN);
            char r = n;
            flags["SF"] = (r < 0);
            flags["ZF"] = (r == 0);
            registers[temp + args[0].at(0) + 'X'].setHigh(r);
        }
        else if (args[0].endsWith('L'))
        {
            short n = registers[temp + args[0].at(0) + 'X'].getLow();
            ++n;
            flags["OF"] = (n > CHAR_MAX || n < CHAR_MIN);
            char r = n;
            flags["SF"] = (r < 0);
            flags["ZF"] = (r == 0);
            registers[temp + args[0].at(0) + 'X'].setLow(r);
        }
        else
        {
            int n = registers[args[0]].get();
            ++n;
            flags["OF"] = (n > SHRT_MAX || n < SHRT_MIN);
            short r = n;
            flags["SF"] = (r < 0);
            flags["ZF"] = (r == 0);
            registers[args[0]].set(r);
        }
    }
    else qDebug() << "Unsupported argument";
}

void Debugger::dec(const Debugger::opArgs args)
{
    auto cf = flags.value("CF"); //CF - unchanged !!!
    resetFlags();
    flags["CF"] = cf;

    aType argT1 = getArgType(args[0]);

    QString temp;

    if(argT1 == reg)
    {
        if (args[0].endsWith('H'))
        {
            short n = registers[temp + args[0].at(0) + 'X'].getHigh();
            --n;
            flags["OF"] = (n > CHAR_MAX || n < CHAR_MIN);
            char r = n;
            flags["SF"] = (r < 0);
            flags["ZF"] = (r == 0);
            registers[temp + args[0].at(0) + 'X'].setHigh(r);
        }
        else if (args[0].endsWith('L'))
        {
            short n = registers[temp + args[0].at(0) + 'X'].getLow();
            --n;
            flags["OF"] = (n > CHAR_MAX || n < CHAR_MIN);
            char r = n;
            flags["SF"] = (r < 0);
            flags["ZF"] = (r == 0);
            registers[temp + args[0].at(0) + 'X'].setLow(r);
        }
        else
        {
            int n = registers[args[0]].get();
            --n;
            flags["OF"] = (n > SHRT_MAX || n < SHRT_MIN);
            short r = n;
            flags["SF"] = (r < 0);
            flags["ZF"] = (r == 0);
            registers[args[0]].set(r);
        }
    }
    else qDebug() << "Unsupported argument";
}

void Debugger::interrupt(const Debugger::opArgs args)
{
    aType argT1 = getArgType(args[0]);

    if(argT1 == num)
    {
        int arg = args[0].toInt(0 , Base);

        if(arg == 0x10) //graphical operations
        {
            unsigned char opCode = getRegVal("AH");

            if(opCode == 0x00) //set screen type
            {
                unsigned char screenMode = getRegVal("AL");

                if (screenMode == 0x02) screen.setScreenType(Screen::text);
                else if (screenMode == 0x0D) screen.setScreenType(Screen::graphics16);
                else if (screenMode == 0x10) screen.setScreenType(Screen::graphics64);

                emit ScreenUpdated();
            }
            else if (opCode == 0x02) //set cursor pos
            {
                screen.setCursorPos(std::make_pair<int, int>(getRegVal("DH"), getRegVal("DL")));
            }
            else if (opCode == 0x06) //cls
            {
                //ToDo : check for screen limits

                screen.cls(getRegVal("CX"), getRegVal("DX"), getRegVal("BH")); //cls from CX to DX
            }
            else if (opCode == 0x0B) //Set palette
            {
                screen.setPalette(getRegVal("BL"));
            }
            else if (opCode == 0x0C) //Draw a point
            {
                screen.drawPoint(getRegVal("DX"), getRegVal("CX"), getRegVal("AL"));
            }
            else qDebug() << "Unsupported argument";
        }
        else if(arg == 0x21) //bios interrupt, IO
        {
            unsigned char opCode = getRegVal("AH");

            if(opCode == 0x1)
            {
                char n = 0;

                emit ScreenUpdated(true);

                if(kbStream != nullptr)
                {
                    kbStream->readAll();

                    while(kbStream->atEnd() && !terminate)
                    {
                        QThread::msleep(125);
                    }
                    *kbStream >> n;
                    screen.print(&n);
                    registers["AX"].setLow(n);
                }
                else qDebug() << "No KB stream set";
            }
            else if(opCode == 0x2) //Print char
            {
                char ch = getRegVal("DL");
                screen.print(&ch);
            }
            else if(opCode == 0x9) //Print string
            {
                int address = getGlobalAddr(getRegVal("DS"), getRegVal("DX"));

                const int EOS = getGlobalAddr(getRegVal("DS"), 0xFFFF); //end of segment

                while(memory[address] != '$' && address <= EOS && !terminate)
                {
                    char ch = memory[address];
                    screen.print(&ch);
                    ++address;
                }
            }
        }
        else if(arg == 0x20) //halt the emulator
        {
            terminate = true;
        }
        else qDebug() << "Unsupported argument";
    }
    else qDebug() << "Unsupported argument";
}

void Debugger::cmp(const Debugger::opArgs args)
{
    resetFlags();

    aType argT1 = getArgType(args[0]);
    aType argT2 = getArgType(args[1]);

    if(argT1 == reg)
    {
        if(args[0].endsWith('H') || args[0].endsWith('L'))
        {
            unsigned char un1 = getRegVal(args[0]);
            unsigned char un2 = 0;
            signed char n1 = un1;
            signed char n2 = 0;

            if(argT2 == num) n2 = un2 = args[1].toInt(0 , Base);
            else if(argT2 == reg) n2 = un2 = getRegVal(args[1]);
            else if (argT2 == mem) n2 = un2 = getMemVal(args[1], 0);
            else qDebug() << "Unsupported argument";

            signed short res = (n1 - n2);
            unsigned short ures = (un1 - un2);

            signed char r = res;
            unsigned char ur = res;

            flags["OF"] = (res > CHAR_MAX || res < CHAR_MIN);
            flags["CF"] = (ur != ures);
            flags["SF"] = (r < 0);
            flags["ZF"] = (r == 0);
        }
        else
        {
            unsigned short un1 = getRegVal(args[0]);
            unsigned short un2 = 0;
            signed short n1 = un1;
            signed short n2 = 0;

            if(argT2 == num) n2 = un2 = args[1].toInt(0 , Base);
            else if(argT2 == reg) n2 = un2 = getRegVal(args[1]);
            else if (argT2 == mem) n2 = un2 = getMemVal(args[1], 1);
            else qDebug() << "Unsupported argument";

            signed int res = (n1 - n2);
            unsigned int ures = (un1 - un2);

            signed short r = res;
            unsigned short ur = res;

            flags["OF"] = (res > SHRT_MAX || res < SHRT_MIN);
            flags["CF"] = (ur != ures);
            flags["SF"] = (r < 0);
            flags["ZF"] = (r == 0);
        }
    }
    else if (argT1 == mem)
    {
        if(argT2 == reg)
        {
            if(args[1].endsWith('H') || args[1].endsWith('L'))
            {
                unsigned char un1 = getRegVal(args[1]);
                unsigned char un2 = getMemVal(args[0], 0);
                signed char n1 = un1;
                signed char n2 = un2;

                signed short res = (n1 - n2);
                unsigned short ures = (un1 - un2);

                signed char r = res;
                unsigned char ur = res;

                flags["OF"] = (res > CHAR_MAX || res < CHAR_MIN);
                flags["CF"] = (ur != ures);
                flags["SF"] = (r < 0);
                flags["ZF"] = (r == 0);
            }
            else
            {
                unsigned short un1 = getRegVal(args[1]);
                unsigned short un2 = getMemVal(args[0], 1);
                signed short n1 = un1;
                signed short n2 = un2;

                signed int res = (n1 - n2);
                unsigned int ures = (un1 - un2);

                signed short r = res;
                unsigned short ur = res;

                flags["OF"] = (res > SHRT_MAX || res < SHRT_MIN);
                flags["CF"] = (ur != ures);
                flags["SF"] = (r < 0);
                flags["ZF"] = (r == 0);
            }
        }
        else qDebug() << "Unsupported argument";
    }
    else qDebug() << "Unsupported argument";
}

void Debugger::jmp(const Debugger::opArgs args)
{
    aType argT1 = getArgType(args[0]);

    if(argT1 == num)
    {
        jumpTo = args[0].toInt(0 , Base);
    }
    else qDebug() << "Unsupported argument";
}

void Debugger::je(const Debugger::opArgs args)
{
    aType argT1 = getArgType(args[0]);

    if(argT1 == num)
    {
        if(flags["ZF"] == true) jumpTo = args[0].toInt(0 , Base);
    }
    else qDebug() << "Unsupported argument";
}

void Debugger::jz(const Debugger::opArgs args)
{
    aType argT1 = getArgType(args[0]);

    if(argT1 == num)
    {
        if(flags["ZF"] == true) jumpTo = args[0].toInt(0 , Base);
    }
    else qDebug() << "Unsupported argument";
}

void Debugger::jne(const Debugger::opArgs args)
{
    aType argT1 = getArgType(args[0]);

    if(argT1 == num)
    {
        if(flags["ZF"] == false) jumpTo = args[0].toInt(0 , Base);
    }
    else qDebug() << "Unsupported argument";
}

void Debugger::jnz(const Debugger::opArgs args)
{
    aType argT1 = getArgType(args[0]);

    if(argT1 == num)
    {
        if(flags["ZF"] == false) jumpTo = args[0].toInt(0 , Base);
    }
    else qDebug() << "Unsupported argument";
}

void Debugger::jg(const Debugger::opArgs args)
{
    aType argT1 = getArgType(args[0]);

    if(argT1 == num)
    {
        if(flags["ZF"] == false && flags["SF"] == flags["OF"]) jumpTo = args[0].toInt(0 , Base);
    }
    else qDebug() << "Unsupported argument";
}

void Debugger::jl(const Debugger::opArgs args)
{
    aType argT1 = getArgType(args[0]);

    if(argT1 == num)
    {
        if(flags["SF"] != flags["OF"]) jumpTo = args[0].toInt(0 , Base);
    }
    else qDebug() << "Unsupported argument";
}

void Debugger::jge(const Debugger::opArgs args)
{
    aType argT1 = getArgType(args[0]);

    if(argT1 == num)
    {
        if(flags["SF"] == flags["OF"]) jumpTo = args[0].toInt(0 , Base);
    }
    else qDebug() << "Unsupported argument";
}

void Debugger::jle(const Debugger::opArgs args)
{
    aType argT1 = getArgType(args[0]);

    if(argT1 == num)
    {
        if((flags["ZF"] == true) || (flags["SF"] != flags["OF"])) jumpTo = args[0].toInt(0 , Base);
    }
    else qDebug() << "Unsupported argument";
}

int Debugger::getGlobalAddr(const unsigned short &seg, const unsigned short &off) const
{
    return ((seg << 4) + off);
}

short Debugger::getRegVal(QString r) const
{
    QString temp;

    if (r.endsWith('H')) return registers.value(temp + r.at(0) + 'X').getHigh();
    else if (r.endsWith('L')) return registers.value(temp + r.at(0) + 'X').getLow();
    else return registers.value(r).get();
}

void Debugger::setRegVal(QString r, unsigned short v)
{
    QString reg(1, r.at(0));
    reg += ('X');

    if (r.endsWith('H')) registers[reg].setHigh(v);
    else if (r.endsWith('L')) registers[reg].setLow(v);
    else registers[r].set(v);
}

short Debugger::getMemVal(const QString& m, char width) const
{
    int address = getGlobalAddr(getRegVal("DS"), getRegVal(m.mid(1, 2)));

    short res = 0;

    if(width == 1)
    {
        res = (memory.at(address) & 0x00ff);
        res |= ((memory.at(++address) & 0x00ff) << 8);
    }
    else if(width == 0)
    {
        res = (memory.at(address) & 0x00ff);
    }

    return res;
}

void Debugger::setMemVal(QString& m, short val, char width)
{
    int address = getGlobalAddr(getRegVal("DS"), getRegVal(m.mid(1, 2)));

    QByteArray temp;

    if(width == 1)
    {
        temp.append(val & 0x00ff);
        temp.append((val & 0xff00) >> 8);

        memory.replace(address, temp.length(), temp);

        emit MemoryUpdated(temp, getRegVal(m.mid(1, 2)));
    }
    else if(width == 0)
    {
        temp.append(val & 0x00ff);

        memory.replace(address, temp.length(), temp);

        emit MemoryUpdated(temp, getRegVal(m.mid(1, 2)));
    }
}

const Screen &Debugger::getScreen() const
{
    return screen;
}

Debugger::aType Debugger::getArgType(const QString &s) const
{
    QRegExp num("[0-9A-F]+");
    QRegExp reg("(?:AX|CX|DX|BX|AH|AL|CH|CL|DH|DL|BH|BL|SP|BP|SI|DI)");
    QRegExp mem("\\[(?:BX|SI|DI)\\]");

    if (num.exactMatch(s)) return aType::num;
    else if (reg.exactMatch(s)) return aType::reg;
    else if (mem.exactMatch(s)) return aType::mem;
    else return undef;
}

void Debugger::resetFlags()
{
    for (auto i = flags.begin(); i != flags.end(); ++i) *i = false;
}

void Debugger::setStream(QTextStream *stream)
{
    kbStream = stream;
}

void Debugger::init(QByteArray * const _memory, QHash<QString, Register> * const segRegisters)
{
    defaultMemoryPtr = _memory;
    defaulSegtRegPtr = segRegisters;

    reset();
}

void Debugger::setErrStream(QTextStream *_errStream)
{
    errStream = _errStream;
}

void Debugger::reset()
{
    terminate = false;
    nextBP = false;

    jumpTo = 0;

    screen.reset();

    for(auto i = registers.begin(); i != registers.end(); ++i)
    {
        i->set(0);
    }

    memory = *defaultMemoryPtr;

    registers.clear();

    registers = *defaulSegtRegPtr;
}

bool Debugger::splitAndExec(QString str)
{
    QStringList strings = str.split(QRegExp("[ ,]"), QString::SkipEmptyParts);

    QString opCode = strings.front();
    QString args[MaxArgs];

    int j = -1;
    for(auto i = strings.begin(); i != strings.end(); ++i)
    {
        if(j >= 0) args[j] = *i;
        ++j;
    }

    if(opCode.isEmpty()) return true;

    (this->*instructions[opCode])(args);
    return false;
}

bool Debugger::parse(const QVector<QPair<int, QString> > &lines)
{
    reset();

    std::unordered_map<int, int> jumps;

    int l = 0;

    for(QPair<int, QString> i : lines)
    {
        jumps.emplace(std::make_pair(i.first, l));
        ++l;
    }

    int line = 1;

    errStream->readAll();

    bool jumped = false;

    int i = 0;

    while(i < lines.size() - 1)
    {
        jumped = false;

        if(!terminate)
        {
            registers["IP"] = lines.at(i).first;

            if(splitAndExec(lines.at(i).second))
                std::cout << "Something went wrong at line " << line << std::endl;

            if(!errStream->atEnd())
            {
                emit error(line);
                break;
            }

            if(jumpTo)
            {
                try
                {
                    i = jumps.at(jumpTo);
                }
                catch(const std::out_of_range& oor)
                {
                    *errStream << "Jumped outside of bounds.";
                }

                registers["IP"] = lines.at(i).first;

                jumpTo = 0;
                jumped = true;
            }

            if(!errStream->atEnd())
            {
                emit FieldsUpdated();
                emit error(line);
                break;
            }

            if(!jumped && !terminate) registers["IP"] = lines.at(i + 1).first;
            if(!jumped) ++i;
            line = i + 1;
        }
        else break;        
    }

    return true;
}

bool Debugger::debug(const QVector<QPair<int, QString> > &lines, const QVector<bool> &bPoints)
{
    reset();

    std::unordered_map<int, int> jumps;

    int l = 0;

    for(QPair<int, QString> i : lines)
    {
        jumps.emplace(std::make_pair(i.first, l));
        ++l;
    }

    int line = 1;

    errStream->readAll();

    bool jumped = false;

    int i = 0;

    while(i < lines.size() - 1)
    {
        jumped = false;

        int prevI = i;

        if(!terminate)
        {
            registers["IP"] = lines.at(i).first;

            if(splitAndExec(lines.at(i).second))
                std::cout << "Something went wrong at line " << line << std::endl;

            if(!errStream->atEnd())
            {
                emit error(line);
                break;
            }

            if(jumpTo)
            {
                try
                {
                    i = jumps.at(jumpTo);
                }
                catch(const std::out_of_range& oor)
                {
                    *errStream << "Jumped outside of bounds.";
                }

                registers["IP"] = lines.at(i).first;

                jumpTo = 0;
                jumped = true;
            }

            if(!errStream->atEnd())
            {
                emit error(line);
                break;
            }

            if(!jumped && !terminate) registers["IP"] = lines.at(i + 1).first;

            if(bPoints.at(prevI))
            {
                while (!nextBP && !terminate)
                {
                    QThread::msleep(16);
                }

                nextBP = false;
            }

            if(!jumped) ++i;
            line = i + 1;
        }
        else break;
    }

    return true;
}

bool Debugger::getFlag(QString f) const
{
    return flags.value(f);
}
