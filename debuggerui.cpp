#include "debuggerui.h"
#include "ui_debuggerui.h"

DebuggerUI::DebuggerUI(QWidget *parent, Qt::WindowFlags f) :
    QWidget(parent),
    ui(new Ui::DebuggerUI),
    kbStream(new QTextStream(&kbBuffer)),
    errStream(new QTextStream(&errBuffer)),
    debug(new Debugger(this, kbStream)),
    screenWidget(new ScreenWidget(this, Qt::Window, kbStream)),
    updateTimer(new QTimer(this))
{
    setWindowFlags(f);
    ui->setupUi(this);

    debug->setErrStream(errStream);

    updateTimer->setInterval(16);

    ui->DShexView->setReadOnly(true);
    ui->DShexView->setShowAscii(false);

    ui->codeView->setFont(QFont("Consolas", 10));
    ui->codeView->setMinimumWidth(ui->codeView->fontMetrics().width('0') * 24);

    connect(debug, SIGNAL(FieldsUpdated()), this, SLOT(updateFields()));
    connect(debug, SIGNAL(ScreenUpdated(bool)), this, SLOT(updateScreen(bool)));
    connect(debug, SIGNAL(MemoryUpdated(QByteArray,int)), ui->DShexView, SLOT(refresh(QByteArray,int)));
    connect(&execWatcher, SIGNAL(finished()), this, SLOT(handleFinished()));    
    connect (debug, SIGNAL(error(int)), this, SLOT(error(int)));
    connect(updateTimer, SIGNAL(timeout()), this, SLOT(timedUpdate()));
}

DebuggerUI::~DebuggerUI()
{
    delete updateTimer;
    delete screenWidget;
    delete debug;
    delete kbStream;
    delete errStream;
    delete ui;
}

void DebuggerUI::init(const QVector<QPair<int, QString>> &lines, QByteArray * const memory, QHash<QString, Register> * const segRegisters)
{
    execWatcher.waitForFinished();

    debug->init(memory, segRegisters);

    src = lines;
    ui->codeView->clear();

    for(auto i = 0; i < src.size() - 1; ++i)
    {
        ui->codeView->addItem("CS:" + QString::number(src.at(i).first, 16).rightJustified(4, '0').toUpper() + " " + src.at(i).second);
    }

    ui->DShexView->initAndLoad(memory, segRegisters->value("DS").get());

    updateScreen();
    updateFields();
}

void DebuggerUI::error(int line)
{
    QString message = "Line " + QString::number(line) + ": " + errStream->readLine();

    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Critical);
    msgBox.setWindowTitle("Error");
    msgBox.setText(message);
    msgBox.exec();
}

void DebuggerUI::run(bool debug, QVector<bool> breakPoints)
{
    if(!execWatcher.isRunning())
    {
        ui->codeView->setDisabled(true);
        ui->DShexView->reload();

        QFuture<bool> future;

        if(debug) future = QtConcurrent::run(this->debug, &Debugger::debug, src, breakPoints);
        else future = QtConcurrent::run(this->debug, &Debugger::parse, src);

        execWatcher.setFuture(future);

        updateTimer->start();
    }
}

void DebuggerUI::updateFields()
{
    auto regList = ui->RegisterGroup->findChildren<QLineEdit *>();

    for(auto i : regList)
    {
        if(i->objectName().endsWith('H') || i->objectName().endsWith('L'))
        {
            i->setText(QString::number(static_cast<unsigned char>(debug->getRegVal(i->objectName())), 16).toUpper().rightJustified(2, '0'));
        }
        else
        {
            i->setText(QString::number(static_cast<unsigned short>(debug->getRegVal(i->objectName())), 16).toUpper().rightJustified(4, '0'));
        }
    }

    auto flagList = ui->FlagGroup->findChildren<QLineEdit *>();

    for(auto i : flagList)
    {
        QString temp;

        i->setText(temp.setNum(debug->getFlag(i->objectName()), 16).toUpper());
    }
}

void DebuggerUI::closeEvent(QCloseEvent *)
{
    if(execWatcher.isRunning()) debug->terminateASAP();
    screenWidget->hide();
}

void DebuggerUI::updateScreen(bool forceShow)
{
    if(forceShow)
    {
        screenWidget->show();
        QPoint temp(mapToGlobal(QPoint(width(),0)));

        screenWidget->move(temp);
    }
    screenWidget->update(debug->getScreen());
}

void DebuggerUI::handleFinished()
{
    updateTimer->stop();
    ui->codeView->setDisabled(false);

    updateScreen();
    updateFields();
}

void DebuggerUI::on_runbtn_clicked()
{
    ui->DShexView->reload();
    run();
}

void DebuggerUI::on_trmbtn_clicked()
{
    if(execWatcher.isRunning()) debug->terminateASAP();
}

void DebuggerUI::on_scrbtn_clicked()
{
    if(screenWidget->isHidden()) screenWidget->show();
    else screenWidget->hide();

    QPoint temp(mapToGlobal(QPoint(width(),0)));

    screenWidget->move(temp);
}

void DebuggerUI::on_dbgbtn_clicked()
{
    QVector<bool> breakPoints(ui->codeView->count());

    for(int j = 0; j < breakPoints.size(); ++j)
    {
        if (ui->codeView->item(j)->isSelected()) breakPoints[j] = true;
    }

    run(true, breakPoints);
}

void DebuggerUI::on_nBPbtn_clicked()
{
    if(execWatcher.isRunning()) debug->next();
}

void DebuggerUI::timedUpdate()
{
    if(updateFinished)
    {
        updateFinished = false;

        updateScreen();
        updateFields();

        updateFinished = true;
    }
}
