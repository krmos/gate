#ifndef COMPILER_H
#define COMPILER_H

#include <unordered_map>
#include <vector>
#include <regex>
#include <QByteArray>
#include <QString>
#include <QStringList>
#include <QRegExp>
#include <QDebug>
#include <QList>
#include <iostream>

class Compiler
{
private:
    enum aType // argument types:
    {
        undef, // could not define type, exception to be thrown
        num, // hex
        reg, // register
        mem, // (complex) memory expression
    };

    const int MaxArgs = 3;
    const int Base = 16;

    int currentAddr = 0x100;

    aType getArgType(const std::string &s) const;

    QTextStream* errStream;
    QString buffer;

    std::unordered_map<std::string, std::vector<unsigned char>> opCodes;
    std::unordered_map<std::string, unsigned char> jmpOps;
    std::unordered_map<std::string, unsigned char> registers;
    std::unordered_map<std::string, unsigned char> memOps;

    QByteArray add(std::string args[]);
    QByteArray sub(std::string args[]);
    QByteArray mov(std::string args[]);
    QByteArray inc(std::string args[]);
    QByteArray dec(std::string args[]);
    QByteArray mul(std::string args[]);
    QByteArray div(std::string args[]);
    QByteArray imul(std::string args[]);
    QByteArray idiv(std::string args[]);
    QByteArray push(std::string args[]);
    QByteArray pop(std::string args[]);
    QByteArray interrupt(std::string args[]);
    QByteArray cmp(std::string args[]);
    QByteArray jmp(std::string args[]);
    QByteArray condJmp(std::string args[]);

public:
    Compiler();
    ~Compiler();

    QByteArray compile(const QStringList &src, QList<int> &opLengths, QStringList &errList);
    QByteArray compileSingleString(const QString &_src);
};

#endif // COMPILER_H
