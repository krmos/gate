#-------------------------------------------------
#
# Project created by QtCreator 2015-12-23T19:33:11
#
#-------------------------------------------------

QT       += core gui concurrent

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = GATE
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    register.cpp \
    debugger.cpp \
    debuggerui.cpp \
    screen.cpp \
    textconsolewidget.cpp \
    graphicsconsolewidget.cpp \
    screenwidget.cpp \
    hexeditor.cpp \
    compiler.cpp \
    codeeditor.cpp \
    hexconverter.cpp \
    asciidelegate.cpp \
    linedelegate.cpp

HEADERS  += mainwindow.h \
    register.h \
    debugger.h \
    debuggerui.h \
    screen.h \
    textconsolewidget.h \
    graphicsconsolewidget.h \
    screenwidget.h \
    hexeditor.h \
    compiler.h \
    codeeditor.h \
    hexconverter.h \
    asciidelegate.h \
    linedelegate.h

CONFIG += c++14

FORMS    += mainwindow.ui \
    debuggerui.ui \
    screenwidget.ui \
    hexconverter.ui

RESOURCES +=
