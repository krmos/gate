#include "hexeditor.h"

HexEditor::HexEditor(QWidget* parent) : QTableWidget(parent),
    delegate(new AsciiDelegate(this)),
    cyrillic(QTextCodec::codecForName("CP-866"))
{
#ifdef Q_OS_WIN32
    setFont(QFont("Consolas", 10));
#endif
#ifdef Q_OS_LINUX
    setFont(QFont("Monospace", 9));
#endif
    connect(this, SIGNAL(cellClicked(int,int)), this, SLOT(tableClicked(int,int)));
    connect(this, SIGNAL(currentCellChanged(int,int,int,int)), this, SLOT(currentCellChanged(int,int,int,int)));
    connect(delegate, SIGNAL(changeRow(int)), this, SLOT(rowChanged(int)));
    connect(delegate, SIGNAL(closeEditor(QWidget*,QAbstractItemDelegate::EndEditHint)), this, SLOT(delegateEditorClosed()));
}

HexEditor::~HexEditor(){}

void HexEditor::keyPressEvent(QKeyEvent *e)
{
    if(selectedItems().size() == 1)
    {
        QTableWidgetItem * item = selectedItems().first();

        int row = item->row();
        int column = item->column();

        const auto Key = e->key();

        if(Key == Qt::Key_Up) changeSelection(--row, column);
        else if(Key == Qt::Key_Down) changeSelection(++row, column);
        else if(Key == Qt::Key_Left) changeSelection(row, --column);
        else if(Key == Qt::Key_Right) changeSelection(row, ++column);
        else if(Key == Qt::Key_Backspace && !readOnly)
        {
            if(column < 0x10)
            {
                QString temp = item->text();

                temp[1] = temp[0];
                temp[0] = '0';

                item->setText(temp);

                if(temp == "00")
                {
                    if (column == 0)
                    {
                        --row;
                        column = 0xF;
                    }
                    else --column;

                    changeSelection(row, column);
                }
            }
        }
        else if (!readOnly)
        {
            if(column < 0x10)
            {
                if(isxdigit(e->key()))
                {
                    QString temp = item->text();

                    temp[0] = temp[1];
                    temp[1] = e->key();

                    item->setText(temp);

                    ++cur;
                }
            }

            if(cur >= 2)
            {
                if (column >= 0xF)
                {
                    ++row;
                    column = 0;
                }
                else ++column;


                changeSelection(row, column);
            }
        }
        else e->ignore();
    }
}

void HexEditor::mouseDoubleClickEvent(QMouseEvent *event)
{
    event->ignore();
}

void HexEditor::init(QByteArray *mem, unsigned int segStart)
{
    clearContents();

    setRowCount(Rows);
    verticalHeader()->setDefaultSectionSize(18);

    if(showAscii)
    {
        setColumnCount(Columns);
        setItemDelegateForColumn(16, delegate);

        setColumnWidth(Columns - 1, 122);
        setHorizontalHeaderItem(Columns - 1, new QTableWidgetItem(QString("ASCII")));
    }
    else setColumnCount(Columns - 1);

    setSelectionMode(QAbstractItemView::SingleSelection);

    horizontalHeader()->setSectionResizeMode(QHeaderView::Fixed);
    verticalHeader()->setSectionResizeMode(QHeaderView::Fixed);

    for(int i = 0; i < 0x10; ++i)
    {
        setColumnWidth(i, 20);
        setHorizontalHeaderItem(i, new QTableWidgetItem(QString::number(i, 16).toUpper()));
    }

    memory = mem;
    segmentStart = (segStart << 4);

    for(int i = 0; i < Rows; ++i)
    {
        setVerticalHeaderItem(i, new QTableWidgetItem(QString::number(segStart, 16).toUpper().rightJustified(4, '0')
                                                      + ':'
                                                      + (QString::number(i  * 16 + Offset, 16).toUpper().rightJustified(4, '0'))));
        for(int j = 0; j < (Columns - 1); ++j)
        {
            setItem(i, j, new QTableWidgetItem(QString("00")));
        }
        if(showAscii)
        {
            setItem(i, Columns - 1, new QTableWidgetItem(QString()));
        }
    }
}

void HexEditor::initAndLoad(QByteArray *mem, unsigned int segStart)
{
    clearContents();

    setRowCount(Rows);
    verticalHeader()->setDefaultSectionSize(18);

    if(showAscii)
    {
        setColumnCount(Columns);
        setItemDelegateForColumn(16, delegate);

        setColumnWidth(Columns - 1, 122);
        setHorizontalHeaderItem(Columns - 1, new QTableWidgetItem(QString("ASCII")));
    }
    else setColumnCount(Columns - 1);

    setSelectionMode(QAbstractItemView::SingleSelection);

    horizontalHeader()->setSectionResizeMode(QHeaderView::Fixed);
    verticalHeader()->setSectionResizeMode(QHeaderView::Fixed);

    for(int i = 0; i < 0x10; ++i)
    {
        setColumnWidth(i, 20);
        setHorizontalHeaderItem(i, new QTableWidgetItem(QString::number(i, 16).toUpper()));
    }

    memory = mem;
    segmentStart = (segStart << 4);

    for(int i = 0; i < Rows; ++i)
    {
        setVerticalHeaderItem(i, new QTableWidgetItem(QString::number(segStart, 16).toUpper().rightJustified(4, '0')
                                                      + ':'
                                                      + (QString::number(i  * 16 + Offset, 16).toUpper().rightJustified(4, '0'))));
        for(int j = 0; j < (Columns - 1); ++j)
        {
            setItem(i, j, new QTableWidgetItem(QString::number(
                                                   static_cast<unsigned char>(memory->at((segmentStart + Offset + (i * 16)) + j)), 16
                                                   ).toUpper().rightJustified(2, '0')));
        }
        if(showAscii)
        {
            setItem(i, 16, new QTableWidgetItem(toReadable(memory->mid(segmentStart + Offset + (i * 16), 16))));
        }
    }
}

void HexEditor::reload()
{
    clearContents();

    for(int i = 0; i < Rows; ++i)
    {
        for(int j = 0; j < 16; ++j)
        {
            setItem(i, j, new QTableWidgetItem(QString::number(
                                                   static_cast<unsigned char>(memory->at((segmentStart + Offset + (i * 16)) + j)), 16
                                                   ).toUpper().rightJustified(2, '0')));
        }
        if(showAscii)
        {
            setItem(i, 16, new QTableWidgetItem(toReadable(memory->mid(segmentStart + Offset + (i * 16), 16))));
        }
    }
}

void HexEditor::update(const QByteArray &ba)
{
    int i = 0;
    int j = 0;

    for(auto it : ba)
    {
        setItem(i, j, new QTableWidgetItem(QString::number(static_cast<unsigned char>(it), 16).toUpper().rightJustified(2, '0')));
        ++j;

        if(j == 16)
        {
            ++i;
            j = 0;
        }
    }

    memory->replace(segmentStart + Offset, ba.length(), ba);

    if (previousSegLength > ba.length())
    {
        int replaced = 0;
        int begin = i * 16 + j;

        while(previousSegLength >= (i * 16 + j))
        {
            setItem(i, j, new QTableWidgetItem(QString("00")));
            ++replaced;
            ++j;

            if(j == 16)
            {
                ++i;
                j = 0;
            }
        }

        QByteArray temp(replaced, 0);

        memory->replace(segmentStart + Offset + begin, temp.length(), temp);
    }

    previousSegLength = ba.length();

    if(showAscii)
    {
        for(int k = 0; k <= i; ++k)
        {
            setItem(k, Columns - 1, new QTableWidgetItem(toReadable(memory->mid(segmentStart + Offset + (k * 16), 16))));
        }
    }
}

void HexEditor::refresh(QByteArray ba, const int pos)
{
    int i = (pos - Offset) / 16;
    int j = (pos - Offset) % 16;

    for(unsigned char it : ba)
    {
        setItem(i, j, new QTableWidgetItem(QString::number(it, 16).toUpper().rightJustified(2, '0')));
        ++j;

        if(j == 16)
        {
            ++i;
            j = 0;
        }
    }
}

void HexEditor::setReadOnly(const bool &b)
{
    readOnly = b;
}

void HexEditor::setShowAscii(const bool &b)
{
    showAscii = b;
}

void HexEditor::tableClicked(int row, int column)
{
    if(column == Columns - 1 && !readOnly)
    {
        QModelIndex index = model()->index(row, column);
        edit(index);
    }
}

void HexEditor::currentCellChanged(int currentRow, int currentColumn, int previousRow, int previousColumn)
{
    if(!readOnly)
    {
        if(previousColumn >= 0 && previousColumn < (Columns - 1))
        {
            QByteArray temp;
            temp.append(item(previousRow, previousColumn)->text().toInt(0, 16));

            memory->replace((segmentStart + Offset + (previousRow * 16) + previousColumn), temp.length(), temp);

            setItem(previousRow, Columns - 1, new QTableWidgetItem(toReadable(memory->mid(segmentStart + Offset + (previousRow * 16), 16))));
        }
        else if (previousColumn == (Columns - 1))
        {
            QByteArray temp = toHex(item(previousRow, previousColumn)->text());

            memory->replace((segmentStart + Offset + (previousRow * 16)), temp.length(), temp);

            for(int i = 0; i < Columns - 1; ++i)
            {
                setItem(previousRow, i, new QTableWidgetItem(QString::number(
                                                                 static_cast<unsigned char>(memory->at((segmentStart + Offset + (previousRow * 16)) + i)), 16
                                                                 ).toUpper().rightJustified(2, '0')));
            }
        }

        if(currentColumn == (Columns - 1))
        {
            QModelIndex index = model()->index(currentRow, currentColumn);
            edit(index);
        }
    }

    cur = 0;
}

void HexEditor::delegateEditorClosed()
{
    if(selectedItems().size() == 1)
    {
        QTableWidgetItem * cell = selectedItems().first();

        int row = cell->row();

        QByteArray temp = toHex(item(row, Columns - 1)->text());

        memory->replace((segmentStart + Offset + (row * 16)), temp.length(), temp);

        for(int i = 0; i < Columns - 1; ++i)
        {
            setItem(row, i, new QTableWidgetItem(QString::number(
                                                             static_cast<unsigned char>(memory->at((segmentStart + Offset + (row * 16)) + i)), 16
                                                             ).toUpper().rightJustified(2, '0')));
        }
    }
}

void HexEditor::rowChanged(int r)
{
    QTableWidgetItem * cell = selectedItems().first();

    int row = cell->row();
    int column = cell->column();

    if (r < 2 && r) changeSelection(row += r, column);
    else if (r == 2)(changeSelection(row, --column));
}

bool HexEditor::changeSelection(int row, int col)
{
    cur = 0;

    if(showAscii)
    {
        if(row >= 0 && row < Rows && col >= 0 && col < Columns)
        {
            QModelIndex next = model()->index(row, col);
            setCurrentIndex(next);
            return false;
        }
        else return true;
    }
    else
    {
        if(row >= 0 && row < Rows && col >= 0 && col < (Columns - 1))
        {
            QModelIndex next = model()->index(row, col);
            setCurrentIndex(next);
            return false;
        }
        else return true;
    }

}

QByteArray HexEditor::toHex(QString str)
{
    return cyrillic->fromUnicode(str);
}

QString HexEditor::toReadable(QByteArray ba)
{
    for(auto i = ba.begin(); i != ba.end(); ++i)
    {
        if(*i == 0x0D || *i == 0x0A) *i = 0;
    }

    QString temp = cyrillic->toUnicode(ba);

    return temp;
}
